from binascii import Error as BinASCIIError, unhexlify
from sys import stderr

import metlinkpid
import serial

with metlinkpid.PID.for_device('/dev/ttyUSB0') as pid:
    print(
        'Enter a message or hexed bytes as indicated by the prompt.\n'
        'Enter blank input to switch between the two data types.\n'
        'Press Ctrl+C to finish.\n'
        '\n'
        'Text messages default to vertical animation with 40 delay (V40) on the first\n'
        'page, and horizontal animation with 0 delay (H0) on subsequent pages.\n'
        'Set custom animation and/or delay by prepending "<animation><delay>^" to the\n'
        'page, where <animation> is "N" (none), "V" (vscroll), or "H" (hscroll)\n'
        'and <delay> is an integer >= 0 and <= 255.\n'
        'Use "_" to move to second row, and "~" to right-justify.\n'
        'Separate multiple pages with "|".'
    )
    want_hexed = False
    try:
        while True:
            print()
            if want_hexed:
                hexed = input('Hexed> ').strip()
                if hexed:
                    # ensure it's hex
                    try:
                        bytes_in = unhexlify(hexed.replace(' ', ''))
                        # ensure it's not a DLE/STX/ETX packet
                        try:
                            try:
                                message = metlinkpid.inspect(bytes_in)
                                print(repr(message))
                            except (ValueError, serial.SerialException) as e:
                                print(e, file=stderr)
                            import dlestxetx
                            from metlinkpid import _crc, _uncrc
                            bytes_in = dlestxetx.encode(bytes_in + _crc(bytes_in))
                            pid.serial.write(bytes_in)
                            response = _uncrc(dlestxetx.read(pid.serial))
                            print(repr(response), file=stderr)
                            # pid.send(message)
                        except (ValueError, serial.SerialException) as e:
                            print(e, file=stderr)
                    except BinASCIIError as e:
                        print(e, file=stderr)
                else:
                    want_hexed = False
            else:
                string = input('Text> ')
                if string:
                    try:
                        pid.send(string)
                    except Exception as e:
                        print(e, file=stderr)
                else:
                    want_hexed = True
    except (KeyboardInterrupt, EOFError):
        print()
