from time import sleep

from metlinkpid import PID
with PID.for_device('/dev/ttyUSB0', ignore_responses=True) as pid:
    pid.send('12:34 FUNKYTOWN~5_Limited Express|_Stops all stations except East Richard')

    while True:
        sleep(10)
        pid.ping()