sudo rm -rf metlink-pi
git clone https://bitbucket.org/jacojac/metlink-pi.git
cd metlink-pi
pip3 install -r requirements.txt
crontab -l > mycron
echo "@reboot sudo python3 /home/pi/metlink-pi/StartWebServer.py &" >> mycron
crontab mycron
rm mycron
cp -v /home/pi/metlink-pi/installers/update.sh /home/pi/update.sh
sudo reboot