from datetime import datetime, tzinfo
from typing import Mapping, Sequence, ClassVar, Optional

import attr
import pyptv3
from dateutil.parser import parse
from dateutil.tz import gettz
from metlinkpid import PID

########################################################################################################################

PID_PORT = '/dev/ttyUSB0'

PTV_DEVELOPER_ID: int = 1000436
PTV_API_KEY: str = '9c51376f-fde2-11e4-9dfa-061817890ad2'

STOP_ID_BOX_HILL: int = 1026
DIRECTION_ID_TRAIN_UP: int = 1

STOP_ID_NELSON_RD_UP: int = 2410
DIRECTION_ID_TRAM_PORT_MELB: int = 38

# If the run ends with these stop IDs, override the destination name.
DEST_NAME_OVERRIDE: Mapping[Sequence[int], str] = {
    (2450,): 'KEW DEPOT',
    (2510,): 'PORT MELB',
    (1162, 1071,): 'FLINDERS ST',
    (1162, 1071, 1181, 1068, 1120, 1155,): 'FLINDERS ST',
    (1162, 1155, 1120, 1068, 1181, 1071,): 'CITY LOOP',
}


########################################################################################################################


@attr.s(frozen=True)
class Departure:
    time_scheduled: datetime = attr.ib()
    time_estimated: Optional[datetime] = attr.ib()
    dest_default: str = attr.ib()
    dest_overridden: str = attr.ib()

    TZ_MELB: ClassVar[tzinfo] = gettz('Australia/Melbourne')

    @classmethod
    def for_stop(cls, stop_id: int, route_type: int, direction_id: int) -> 'Departure':
        client = pyptv3.Client(developer_id=str(PTV_DEVELOPER_ID), api_key=PTV_API_KEY)
        departures = pyptv3.Departures(client, route_type, stop_id).all(direction_id=direction_id, max_results=1,
                                                                        expand='run')
        departure = departures['departures'][0]
        run_id = departure['run_id']
        stop_departures = pyptv3.Patterns(client=client, run_id=run_id, route_type=route_type).all()['departures']
        stop_ids = tuple([departure['stop_id'] for departure in stop_departures])
        time_scheduled = cls._dt_from_str(departure['scheduled_departure_utc'])
        try:
            stop_departure = next(
                stop_departure
                for stop_departure in stop_departures
                if stop_departure['stop_id'] == stop_id and stop_departure['estimated_departure_utc']
            )
            time_estimated = cls._dt_from_str(stop_departure['estimated_departure_utc'])
        except StopIteration:
            time_estimated = None
        dest_default = departures['runs'][str(run_id)]['destination_name']
        dest_overridden = cls._dest_for_stop_ids(stop_ids, default=dest_default)
        return Departure(time_scheduled, time_estimated, dest_default, dest_overridden)

    @classmethod
    def _dest_for_stop_ids(cls, stop_ids: Sequence[int], default: str) -> str:
        stops_str = '/'.join(tuple(str(stop) for stop in stop_ids))
        for ending_stops, overridden in DEST_NAME_OVERRIDE.items():
            ending_stops_str = '/'.join(tuple(str(stop) for stop in ending_stops))
            if stops_str.endswith(ending_stops_str):
                return overridden
        return default

    @classmethod
    def _dt_from_str(cls, string: str) -> datetime:
        return parse(string).astimezone(cls.TZ_MELB)

    def __str__(self):
        time_due = self.time_estimated if self.time_estimated else self.time_scheduled
        dep_mins = max([0, int((time_due - datetime.now(self.TZ_MELB)).total_seconds() / 60)])
        return '{} {}~ {}'.format(
            self.time_scheduled.strftime('%I:%M').lstrip('0'),
            self.dest_overridden.split('/')[0].upper(),
            'NOW' if dep_mins == 0 else str(dep_mins),
        )


departures = [
    Departure.for_stop(STOP_ID_NELSON_RD_UP, pyptv3.RouteTypes.TRAM, DIRECTION_ID_TRAM_PORT_MELB),
    Departure.for_stop(STOP_ID_BOX_HILL, pyptv3.RouteTypes.TRAIN, DIRECTION_ID_TRAIN_UP),
]
sorted_departures = sorted(departures, key=lambda d: d.time_estimated if d.time_estimated else d.time_scheduled)
for departure in sorted_departures:
    print(str(departure))
with PID.for_device(PID_PORT) as pid:
    pid.send('_'.join(str(departure) for departure in sorted_departures))
